<?php
/**
 * @author      Jesse Boyer <contact@jream.com>
 * @copyright   Copyright (C), 2013 Jesse Boyer
 * @license     GNU General Public License 3 (http://www.gnu.org/licenses/)
 *              Refer to the LICENSE file distributed within the package.
 *
 * @link        http://jream.com
 * 
 * @category    Uri
 * 
 * Retrieves parts of the URI, usage:
 * 
 *  jream\Uri::first();
 *  jream\Uri::last();
 *  jream\Uri::part(2);
 */
namespace jream;
class Uri
{
    // ------------------------------------------------------------------------
    
    /**
     * part - Returns a part of the URI
     * 
     * @param integer $int Part of the relative URI to fetch between slashes /
     * 
     * @return mixed|boolean
     */
    public static function getPart($int)
    {
        $uri = self::_getUri();
        return isset($uri[$int]) ? $uri[$int] : false;
    }
    
    // ------------------------------------------------------------------------
    
    /**
     * Get the first URI item
     * 
     * @return mixed|boolean
     */
    public static function getFirst()
    {
        $uri = self::_getUri();
        return isset($uri[0]) ? $uri[0] : false;
    }
    
    // ------------------------------------------------------------------------
    
    /**
     * Get the last URI item
     * 
     * @return mixed|boolean
     */
    public static function getLast()
    {
        $uri = self::_getUri();
        return !empty($uri) ? end($uri) : false;
    }
    
    // ------------------------------------------------------------------------
    
    /**
     * Get the entire URI in array format
     * 
     * @return array|boolean
     */
    public static function getAll()
    {
        $uri = self::_getUri();
        return !empty($uri) ? $uri : false;
    }
    
    // ------------------------------------------------------------------------
    
    /**
     * Santizes and returns an array formatted URI
     * 
     * @return array
     */
    private static function _getUri()
    {
        
        /** Prevent a null-byte from going through */
        $uri = filter_var($_SERVER['REQUEST_URI'], FILTER_SANITIZE_URL);

        /** Prevent a slash "/" from breaking the array below */
        $uri = rtrim($uri);
        
        /** Break up the URL */
        return explode('/', $uri);
    }
    
    // ------------------------------------------------------------------------
    
    /**
     * If using MVC: Get the current URI Controller (if one is in action)
     * 
     * @return string|boolean
     */
    public static function getCurrentController()
    {
        if (class_exists('\jream\mvc\Bootstrap')) {
            return \jream\mvc\Bootstrap::$currentController;
        }
        
        return false;
    }
    
    // ------------------------------------------------------------------------
    
    /**
     * If using MVC: Get the current URI Method (if one is in action)
     * 
     * @return string|boolean
     */
    public static function getCurrentMethod()
    {
        if (class_exists('\jream\mvc\Bootstrap')) {
            return \jream\mvc\Bootstrap::$currentMethod;
        }
        
        return false;
    }
    
    // ------------------------------------------------------------------------
    
}
/** eof */