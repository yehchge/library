<html>
<body onload="init()">
  
<?php

require_once '../jream/autoload.php';
new jream\Autoload('../jream/');

echo "Run from CMD: $ php socket_send.php\n";

$socket = new jream\Socket('127.0.0.1', 99);
$socket->send('Hello');

?>

<script>
// Using HTML5 Websockets, this will create an additional socket
// 1. Connect via PHP Socket
// 2. Connect via HTML5 Web Socket
//function init() {
//    var connection = new WebSocket('ws://127.0.0.1:99', ['soap', 'xmpp']);
//    
//    connection.onopen = function () {
//        console.log('Ping');
//        connection.send('Ping'); // Send the message 'Ping' to the server
//    };
//
//    connection.onerror = function (error) {
//        console.log('WebSocket Error ' + error);
//        console.log(error);
//    };
//
//    connection.onmessage = function (e) {
//        console.log('Server: ' + e.data);
//    };
//}
</script>

</body>
</html>